<?php

/**
 * @module         SimplePageHead
 * @version        see info.php of this module
 * @authors        cms-lab (based on an idea by CHIO)
 * @copyright      2023-2025 cms-lab
 * @license        GNU General Public License
 * @license terms  see info.php of this module
*/


class simplepagehead extends LEPTON_abstract
{	
	public array $all_sections = [];
	public array $aExtInfo = [];
	public array $aPageInfo = [];
	public string $title = PAGE_TITLE;
	public string $description = DESCRIPTION;
	public string $keywords = KEYWORDS;
	public string $tags = '';
	
	public LEPTON_database $database;

	static $instance;	

	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();
		$this->aPageInfo = [
			'title'=> $this->title,
			'description'=> $this->description,
			'keywords'=> $this->keywords,
			'tags'=> $this->tags
		];
	}
	
	public function init_snippet( $iPageId = -1 )
	{	
		$this->database->execute_query(
			"SELECT DISTINCT module, section_id FROM ".TABLE_PREFIX."sections WHERE page_id = ".$iPageId,
			true,
			$this->all_sections,
			true
		);

		$addons = [];
		foreach($this->all_sections as $info)
		{
			if($info['module'] != 'wysiwyg')
			{
				$addons[] = ['module' => $info['module'], 'section_id' => $info['section_id'] ];
			}
		}

		$all_tools = [];
		$this->database->execute_query(
			"SELECT directory FROM ".TABLE_PREFIX."addons WHERE `function` = 'tool'",
			true,
			$all_tools,
			true
		);
		
		foreach($all_tools as $current_tool)
		{		
			if(file_exists(LEPTON_PATH.'/modules/'.$current_tool['directory'].'/classes/'.$current_tool['directory'].'_simplepagehead.php'))
			{
				$addons[] = ['module' => $current_tool['directory']];
			}
		}			
		
		foreach($addons as $lookup)
		{
			if(class_exists($lookup['module'].'_simplepagehead_custom'))
			{				
				$class = $lookup['module'].'_simplepagehead_custom';
				$this->aExtInfo = $class::get_addon_infos();
				break;
			}			
			elseif(class_exists($lookup['module'].'_simplepagehead'))
			{

				$class = $lookup['module'].'_simplepagehead';
				$this->aExtInfo = $class::get_addon_infos();
				break;				
			}	
		}
	}	

	public function display_head( $iPageId = -1)
	{
        if ((is_numeric($iPageId)) && ($iPageId != -1))
		{
			if(!empty($this->aExtInfo))
			{
				$head = $this->aExtInfo;
			}
			else
			{
				$head = $this->aPageInfo;
			}

			// data for twig template engine	
			$data = [
				'head'		=> $head,
				'language'	=> strtolower(LANGUAGE),
				'website_title'	=> WEBSITE_TITLE,
				'DEFAULT_TEMPLATE'	=> DEFAULT_TEMPLATE
			];

			$template = 'output';
			if(file_exists(LEPTON_PATH.'/modules/simplepagehead/templates/output_custom.lte'))
			{
				$template = 'output_custom';
			}

			 // get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('simplepagehead');
				
			echo $oTwig->render( 
				"@simplepagehead/".$template.".lte",	//	template-filename
				$data					//	template-data
			);
		}
	}		
}