### simplepagehead

Snippet to generate head tags (title, keywords.. )

#### Requirements

* [LEPTON CMS][1], Version see precheck.php

#### Installation

* download latest [.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### What it does:

Displays a complete head section, except stylesheet and script links for all pages but particularly for news, bakery, gallery and topics module pages.<br>
If possible it replaces the title and meta description by the module item title and item (short-)description. <br><br>

Displays a link to the favicon in the root of your installation or wherever your favicon is located. Just edit the output template file.<br><br>

NOTE: The snippet is a replacement for all head tags.<br>
You have to replace all relevant tags in your the old head section.<br>

#### Using the snippet function:

Once the snippet is installed, it can be called from the index.php file of your template.<br><br>

From template index.php
```
...
<head>
<?php simplepagehead(); ?>
<link rel="stylesheet" type="text/css" href=".....
...
```
#### Additional Parameters:

You can completely modify the standard output wiht a output_custom.lte in the templatefolder of somplepagehead.<br>
For a start copy the output.lte, rename it to output_custom.lte and adapt the output to your needs.<br><br>


#### Trouble shooting:
  
If you run into problems feel free to use the [forum][4]. <br>


#### NEW:
  
Starting with this release other addons have to deliver data for the simplepagehead snippet.<br>
To connect your addon see details in the file <b>/modules/news/classes/news_simplepagehead.php</b>.<br>
For additional infos please see [the docs][5]. <br>

  
#### Notice: 
   
If you want to use no-robots tags please use special addon [Meta Tag Index][3]<br><br>

[1]: https://lepton-cms.org
[2]: https://lepton-cms.com/lepador/snippets/simplepagehead.php
[3]: https://lepton-cms.com/lepador/admintools/meta-tag-index.php
[4]: https://forum.lepton-cms.org/
[5]: https://doc.lepton-cms.org/docu/english/addons/connect-with-simplepagehead.php
