<?php

/**
 * @module         SimplePageHead
 * @version        see info.php of this module
 * @authors        cms-lab (based on an idea by CHIO)
 * @copyright      2023-2025 cms-lab
 * @license        GNU General Public License
 * @license terms  see info.php of this module
*/



// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "..*/";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include ".SEC_FILE."!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$module_directory     = 'simplepagehead';
$module_name          = 'SimplePageHead';
$module_function      = 'snippet';
$module_version       = '2.2.1';
$module_platform      = '7.x';
$module_author        = 'cms-lab';
$module_license       = 'GNU General Public License';
$module_terms         = 'none';
$module_description   = 'Snippet to generate head tags (title, keywords.. ) better, simpler and also on newspages, bakery, topics and other modules.';
$module_guid          = 'b29996e9-6c1a-409a-a698-ccb90ef97793';
